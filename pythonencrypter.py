# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 20:33:19 2015

@author: Christopher
"""

import re

def sexystripper(obj):
    obj = re.sub('[\s+]', "", obj)
    obj = re.sub('[,]', "", obj)
    obj = re.sub('[.]', "", obj)
    obj = re.sub('["]', "", obj)
    obj = re.sub('[=]', "", obj)
    obj = re.sub('[+]', "", obj)
    obj = re.sub('[-]', "", obj)
    obj = re.sub('[\']', "", obj)
    obj = re.sub('[;]', "", obj)
    obj = re.sub('[:]', "", obj)
    return obj
    
#http://stackoverflow.com/questions/8886947/caesar-cipher-function-in-python	
def ceasar(obj):
    ceasarstext = ""
    for ch in obj:
        if ch.isalpha():
            mysteryword = ord(ch) + 5
            if mysteryword > ord('z'):
                mysteryword -= 26
            outputletter = chr(mysteryword)
        ceasarstext += outputletter
    return ceasarstext
            

file =  open('C:\\Users\\Christopher\\Documents\\stories\\excerpt.txt')
outputfile1 = open("stripped-file.txt", "w")
outputfile2 = open("cypher.txt", "w")
thing = file.read() 
nakedfile = str(sexystripper(thing))
outputfile1.write(nakedfile)
cryptedfile = str(ceasar(nakedfile))
outputfile2.write(cryptedfile)
print (cryptedfile)