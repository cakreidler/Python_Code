# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
def steelbeamv():
    print ('|   ', end=" ")

def steelbeamh():
    print ('+---', end=" ")

def do4(f):
    do2(f)
    do2(f)

def do2(f):
    f()
    f()
    
def makeh():
    do2(steelbeamh)
    print ('+')

def makev():
    do2(steelbeamv)
    print ('|')

def maker():
    makeh()
    do4(makev)
    
def makesqr():
    do2(maker)
    makeh()
    
makesqr()